import requests
from lxml import etree
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import json
import time


"""

       电影数据爬取(以豆瓣为例)

                                  """

# 爬虫代码1:
#
# url = 'https://movie.douban.com/j/chart/top_list'
#
# params = {
#         'type':'23',
#         'interval_id':'100:90',
#         'action':'',
#         'start':'1',
#         'limit':'100',
# }
#
# headers = {
#         'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Edg/94.0.992.38'
# }
# data_movie = requests.get(url=url, headers=headers,params=params).json()
#
# 爬虫代码2:
#
headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36 Edg/97.0.1072.76'
}
all_dy = []
all_movie_name = []
all_rating=[]
all_vote_count = []
all_url = []
all_actor = []
all_type = []
all_time = []

start_time = time.time()
for i in range(0,226,25):
    url = 'https://movie.douban.com/top250?start={0}&filter='.format(i)
    data_text = requests.get(url=url, headers=headers).text
    # print(data_text)
    tree = etree.HTML(data_text)
    all_data = tree.xpath('//div[@id="content"]/div/div/ol/li')
    for j in all_data:
        new_url = j.xpath('./div/div/a/@href')[0]
        all_url.append(new_url)
        detail_data = requests.get(url=new_url, headers=headers).text
        tree = etree.HTML(detail_data)
        new_data = tree.xpath('//a[@rel="v:starring"]')
        new_t = tree.xpath('//span[@property="v:genre"]')
        # 电影名
        detail_name = tree.xpath('//div[@id="content"]/h1/span/text()')[0]
        all_movie_name.append(detail_name)
        print(detail_name)
        # 评分
        detail_rating = tree.xpath('//div[@id="interest_sectl"]/div[1]/div[2]/strong/text()')[0]
        all_rating.append(detail_rating)
        print(detail_rating)
        # 观看人数
        vote_count = tree.xpath('//*[@id="interest_sectl"]/div[1]/div[2]/div/div[2]/a/span/text()')[0]
        all_vote_count.append(vote_count)
        print(vote_count)
        # 导演
        detail_dy = tree.xpath('//*[@id="info"]/span[1]/span[2]/a/text()')[0]
        all_dy.append(detail_dy)
        print(detail_dy)
        # 演员
        actors = []
        for span in new_data:
            actor = span.xpath('./text()')[0]
            actors.append(actor)
        all_actor.append(actors)
        print(actors)
        # 类型
        types_ = []
        for k in new_t:
            type_ = k.xpath('./text()')[0]
            types_.append(type_)
        all_type.append(types_)
        print(types_)
        # 时长
        runtime_ = tree.xpath('//span[@property = "v:runtime"]/text()')[0]
        all_time.append(runtime_)
        print(runtime_)
        # time.sleep(5)
    # time.sleep(10)
    print('前{0}部爬取结束!'.format(i+25))
    time.sleep(300)

end_time = time.time()
print('爬虫所需时间:\n',start_time - end_time)

data_movie = {
    'url':all_url,
    'director':all_dy,
    'rating':all_rating,
    'title':all_movie_name,
    'vote_count':all_vote_count,
    'actor':all_actor,
    'type':all_type,
    'runtime':all_time,
}
#
# # 持久化存储
# fp = open('电影数据.json', 'w', encoding='utf-8')
# json.dump(data_movie, fp=fp, ensure_ascii=False)
# print('爬取结束!')
#
data_new = pd.DataFrame(data_movie)
#
# # 保存数据 utf_8_sig解决中文乱码问题
data_new.to_csv('D:\数据\豆瓣电影TOP250_Pro.csv',encoding='utf_8_sig')

"""
      电影数据分析及可视化
                           """


# 解决中文乱码问题
# plt.rcParams['font.sans-serif'] = ['SimHei']
# plt.rcParams['axes.unicode_minus'] = False
#
# # 读取保存的csv中的数据
# # data = pd.read_csv('D:\数据\豆瓣电影TOP250.csv')
# # json_data = pd.read_json('')
# #
# # 计算100部电影评分的平均分
# print('100部电影评分的平均分: \n',data['score'].mean())
#
# # 演员人数
# actors_num = np.unique(data['actors']).size
# print('演员人数：\n',actors_num)
#
#
# # 查看电影评分分布情况
# # 创建画布
# plt.figure(figsize=(20,12),dpi=80)
# ax1 = plt.subplot(2,2,1)
# ax2 = plt.subplot(2,2,2)
# ax3 = plt.subplot(2,2,3)
# ax4 = plt.subplot(2,2,4)
#
# # 选择ax2
# plt.sca(ax2)
# plt.title('电影评分分布情况',fontsize=20)
# # 绘制直方图
#
# plt.hist(data['score'],bins=10)
# #修改刻度
# plt.xticks(np.linspace(data['score'].min(),data['score'].max(),num=11)) # np.linspace生成等差数列 num:样本量
# # 显示网格
# plt.grid()
#
# # 选择ax4
# plt.sca(ax4)
# plt.title('前十部电影评分',fontsize=20)
# # 绘制柱状图
# plt.bar(range(10),data['score'][:10],color=['r','g','b','c','m','y','k','grey','gold','tan'],
#         width=.5)
# plt.ylabel('评分',fontsize=15)
# plt.xlabel('电影名称',fontsize=15)
# #修改刻度
# plt.xticks(range(10),data['title'][:10],rotation=75)
#
# # 统计电影所属国家
# # 数据处理
# moive_country = [i.split("['")[1].split("']")[0] for i in data['regions']] # 国家数据切片
# data['regions'] = moive_country
# # 统计国家类别
# Country = np.unique([i for i in moive_country])
# # 生成DataFrame
# count = pd.DataFrame(np.zeros(shape=[100,len(Country)],dtype='int32'),columns=Country)
# # 填充数据
# for i in range(100):
#     count.loc[i,moive_country[i]]= 1
#
# country_type = count.sum() # 按列求和
# # 准备画图
# # 选择ax3
# plt.sca(ax3)
# x_ticks = range(len(country_type.index))
# plt.bar(x_ticks,country_type.values)
# plt.xticks(x_ticks,country_type.index,rotation=90)
# plt.yticks(range(0,71,5))
#
# # 对json文件数据进行解析
# # 统计电影类型
# movie_types = [i for i in json_data['types']]
# all_types = np.unique([j for i in movie_types for j in i])
# print('所有电影类型:\n',all_types)
# # 统计每个类别有几个电影
# count = pd.DataFrame(np.zeros(shape=[100,len(all_types)],dtype='int32'),columns=all_types)
# for i in range(100):
#     count.loc[i,movie_types[i]] = 1
# # print(count)
# num = count.sum(axis=0)# 按列求和
# # 绘制饼图
# # 选择ax1
# plt.sca(ax1)
# plt.pie(num.values,labels=num.index,autopct='%1.2f%%')
# # 显示图例
# plt.legend()
# plt.axis('equal')
# plt.show()